use std::env;
use std::fs::File;
use std::io::{prelude::*, BufReader};
// https://adventofcode.com/2019/day/1

fn main() {
    let mut total_fuel: i32 = 0;
    let input = read_input_file("input.txt");
    let question_part = env::args().nth(1).expect("missing argument");

    for module_mass in input.lines() {
        // Convert input str to integer.
        let module_mass = module_mass.unwrap().parse::<i32>().unwrap();

        let mut module_fuel = fuel_calculator(module_mass);
        total_fuel = total_fuel + module_fuel;

        if question_part == "2" {
            // Calculate fuel required for fuel mass.
            while module_fuel > 0 {
                module_fuel = fuel_calculator(module_fuel);
                if module_fuel > 0 {
                    total_fuel = total_fuel + module_fuel;
                }
            }
        }
    }

    println!(
        "The answer to part {} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        total_fuel
    );
}

/// Calculate fuel for given mass.
///
/// Fuel required to launch a given module is based on its mass.
/// Specifically, to find the fuel required for a module, take its mass,
/// divide by three, round down, and subtract 2.
fn fuel_calculator(mass: i32) -> i32 {
    mass / 3 - 2
}

/// Reads input text file.
/// # Arguments
/// * `file_name` - Name of file to read. Should be located in package root directory.
fn read_input_file(file_name: &str) -> BufReader<File> {
    let file_path = format!("{}/{}", env!("CARGO_PKG_NAME"), file_name);
    let file = File::open(file_path).unwrap();
    BufReader::new(file)
}
