use std::env;

// https://adventofcode.com/2019/day/4

fn main() {
    let question_part = env::args().nth(1).expect("missing argument");
    let answer: u32;
    let start = 264360;
    let end = 746325;
    match question_part.as_str() {
        "1" => answer = password_checker(start, end, 1),
        "2" => answer = password_checker(start, end, 2),
        _ => panic!("Not a valid question number."),
    }
    println!(
        "The answer to part {} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Count how many different passwords wihtin the range meet the criteria.
fn password_checker(start: u32, end: u32, part: u32) -> u32 {
    let mut count: u32 = 0;
    for number in start..end {
        if part == 1 {
            if adjacency_checker(number) && increase_checker(number) {
                count += 1;
            }
        }
        if part == 2 {
            if adjacency_checker_part2(number) && increase_checker(number) {
                count += 1;
            }
        }
    }
    count
}

/// Check that 2 adjacent digits are equal within given integer.
fn adjacency_checker(number: u32) -> bool {
    let mut digits = digits(number);
    let mut check = digits.next();
    for digit in digits {
        if digit == check.unwrap() {
            return true;
        } else {
            check = Some(digit);
        }
    }
    false
}

// Check that two adjacent matching digits are not part of a larger group of matching digits.
fn adjacency_checker_part2(number: u32) -> bool {
    let digits = digits(number);
    let mut check = 0;
    let mut count = 0;
    let mut counts: Vec<u32> = Vec::new();
    for (index, digit) in digits.enumerate() {
        if index == 0 {
            check = digit;
            count += 1;
        } else if digit == check {
            count += 1;
        } else {
            counts.push(count);
            count = 1;
            check = digit;
        }
    }
    counts.push(count);
    counts.iter().any(|&x| x == 2)
}

/// Check that digits never decrease going from left to right within given integer.
fn increase_checker(number: u32) -> bool {
    let mut digits = digits(number);
    let mut check = digits.next();
    for digit in digits {
        if digit >= check.unwrap() {
            check = Some(digit);
            continue;
        } else {
            return false;
        }
    }
    true
}

/// Return iterator for each digit from given integer.
fn digits(number: u32) -> impl Iterator<Item = u32> {
    number
        .to_string()
        .chars()
        .map(|d| d.to_digit(10).unwrap())
        .collect::<Vec<_>>()
        .into_iter()
}
