extern crate array_tool;
use array_tool::vec::Intersect;
use helper;
use std::cmp::Ordering;
use std::env;

#[derive(Debug, Copy, Clone)]
struct Point {
    x: i32,
    y: i32,
}

// https://adventofcode.com/2019/day/3

impl Ord for Point {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.x).cmp(&other.x)
    }
}

impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl Eq for Point {}

impl Point {
    fn move_right(&mut self) {
        self.x += 1;
    }
    fn move_left(&mut self) {
        self.x -= 1;
    }
    fn move_up(&mut self) {
        self.y += 1;
    }
    fn move_down(&mut self) {
        self.y -= 1;
    }
    /// Return distance from origin along wire.
    fn distance_from_origin(&self) -> i32 {
        &self.x.abs() + &self.y.abs()
    }
}

fn main() {
    let input = helper::read_file("day03/input.txt");
    let (wire_1, wire_2) = split_string(input);

    let wire_1 = wire_points(&wire_1);
    let wire_2 = wire_points(&wire_2);

    let question_part = env::args().nth(1).expect("missing argument");
    let answer: i32;
    match question_part.as_str() {
        "1" => answer = part_01(wire_1, wire_2),
        "2" => answer = part_02(wire_1, wire_2),
        _ => panic!("Not a valid question number."),
    }
    println!(
        "The answer to part {} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Part 1: What is the Manhattan distance from the central
/// port to the closest intersection?
fn part_01(wire_1: Vec<Point>, wire_2: Vec<Point>) -> i32 {
    let intersections = wire_1.intersect(wire_2);
    get_shortest_path(intersections)
}

fn part_02(wire_1: Vec<Point>, wire_2: Vec<Point>) -> i32 {
    fewest_steps(&wire_1, &wire_2)
}

/// Split 2 lines in input.txt into individual strings.
fn split_string(input: String) -> (String, String) {
    let strings: Vec<&str> = input.split("\r\n").collect();
    (strings[0].to_string(), strings[1].to_string())
}

/// Extract distance from single straight wire run.
///
/// #Arguments
/// * `wire_run` - Straight wire run in the form: 'U2' or 'R10', etc.
fn extract_distance(wire_run: &str) -> u32 {
    wire_run[1..].parse::<u32>().unwrap()
}

/// Extract direction (up/down/left/right) from signle straight wire run.
///
/// #Arguments
/// * `wire_run` - Straight wire run in the form: 'U2' or 'R10', etc.
fn extract_direction(wire_run: &str) -> char {
    wire_run.chars().nth(0).unwrap()
}

/// Create vector of each point a wire passes on the grid.
///
/// # Arguments
/// * `wire` - Wire input string, in form:  R75,D30,R83,U83...
fn wire_points(wire: &String) -> Vec<Point> {
    let runs: Vec<&str> = wire.split(",").collect();
    let mut points: Vec<Point> = Vec::new();
    let mut point = Point { x: 0, y: 0 };

    for run in runs {
        let direction = extract_direction(run);
        let distance = extract_distance(run);
        for _ in 0..distance {
            match direction {
                'L' => point.move_left(),
                'R' => point.move_right(),
                'D' => point.move_down(),
                'U' => point.move_up(),
                _ => (),
            }
            points.push(point);
        }
    }
    points
}

/// Return wire length for intersection point which is closest to origin.
///
/// #Arguments
/// * `intersections` - Vector<Point> of all points of intersections between wires.
fn get_shortest_path(intersections: Vec<Point>) -> i32 {
    let mut shortest_path: Option<i32> = None;
    for i in &intersections {
        if shortest_path == None {
            shortest_path = Some(i.distance_from_origin());
        } else if i.distance_from_origin() < shortest_path.unwrap() {
            shortest_path = Some(i.distance_from_origin());
        }
    }
    shortest_path.unwrap()
}

/// What is the fewest combined steps the wires must take to reach an intersection?
fn fewest_steps(wire_1: &Vec<Point>, wire_2: &Vec<Point>) -> i32 {
    let mut steps: Option<i32> = None;
    for (steps_1, i) in wire_1.iter().enumerate() {
        for (steps_2, j) in &mut wire_2.iter().enumerate() {
            if i == j {
                let distance = steps_1 as i32 + steps_2 as i32 + 2;
                if steps == None {
                    steps = Some(distance);
                } else if distance < steps.unwrap() {
                    steps = Some(distance);
                }
            }
        }
    }
    steps.unwrap()
}
