use helper;
use std::collections::HashMap;

fn main() {
    let input = helper::read_input_file_to_str(env!("CARGO_PKG_NAME"), "input.txt");
    let question_part = helper::get_question_part();
    let answer: u32;
    match question_part {
        helper::QuestionPart::Part1 => {
            let orbits = get_orbits(input);
            answer = total_orbits(orbits);
        }
        helper::QuestionPart::Part2 => {
            let orbits = get_orbits(input);
            answer = orbits_to_santa(&orbits);
        }
    }
    println!(
        "The answer to part {:?} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Create hashmap of orbit pairs: {satellite: parent body}.
///
/// # Arguments
/// * `input_lines` - Orbit pairs as String in form of X)Y where Y orbits X. Each orbit seperated by newline.
fn get_orbits(input_lines: String) -> HashMap<String, String> {
    let mut orbits = HashMap::new();
    for orbit_pair in input_lines.lines() {
        let orbit_pair: Vec<&str> = orbit_pair.split(")").collect();
        // index 1 = orbiting body, index 0 = parent body
        orbits.insert(orbit_pair[1].to_string(), orbit_pair[0].to_string());
    }
    orbits
}

fn total_orbits(orbits: HashMap<String, String>) -> u32 {
    let mut total_orbit_count: u32 = 0;
    for satellite in orbits.keys() {
        total_orbit_count += indirect_orbit_count(satellite.to_string(), &orbits)
    }
    total_orbit_count
}

/// Get count of all indirect orbits. Using satellite as starting point, step through
/// each parent body until no parent bodies found (reach COM).
///
/// # Arguments
/// * `satellite` - Starting point to count indirect orbits.
/// * `orbits` - Hashmap of all orbits.
fn indirect_orbit_count(mut satellite: String, orbits: &HashMap<String, String>) -> u32 {
    let mut count: u32 = 0;
    while let Some(parent) = orbits.get(&satellite) {
        count += 1;
        satellite = parent.to_string();
    }
    count
}

/// Get vector of all indirect orbits for given satellite as starting point.
///
/// # Arguments
/// * `satellite` - Starting point to count indirect orbits.
/// * `orbits` - Hashmap of all orbits.
fn indirect_orbit_vector(mut satellite: String, orbits: &HashMap<String, String>) -> Vec<String> {
    let mut indirect_orbits: Vec<String> = Vec::new();
    while let Some(parent) = orbits.get(&satellite) {
        satellite = parent.to_string();
        indirect_orbits.push(parent.to_string());
    }
    indirect_orbits
}

/// Get minimum number of orbital transfers required to move from YOU are orbiting to the object SAN
/// is orbiting.
/// Create list of indirect orbits for both, then find first common orbit and sum up transfers between both.
fn orbits_to_santa(orbits: &HashMap<String, String>) -> u32 {
    let orbits_santa = indirect_orbit_vector(String::from("SAN"), orbits);
    let orbits_you = indirect_orbit_vector(String::from("YOU"), orbits);

    for (index_santa, orbit_santa) in orbits_santa.iter().enumerate() {
        for (index_you, orbit_you) in orbits_you.iter().enumerate() {
            if orbit_santa == orbit_you {
                return (index_you + index_santa) as u32;
            }
        }
    }

    panic!("Could not find common orbit between you and Santa.")
}
