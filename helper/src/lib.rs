#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

use std::env;
use std::fs;
use std::fs::File;
use std::io::BufReader;

#[derive(Debug)]
pub enum QuestionPart {
    Part1,
    Part2,
}

pub fn read_file(path: &str) -> String {
    let input_file = fs::read_to_string(path).unwrap();
    String::from(&input_file[..])
}

/// Reads input text file.
/// # Arguments
/// * `file_name` - Name of file to read. Should be located in package root directory.
pub fn read_input_file(package_name: &str, file_name: &str) -> BufReader<File> {
    let file_path = format!("{}/{}", package_name, file_name);
    let file = File::open(file_path).unwrap();
    BufReader::new(file)
}

/// Reads input text file.
/// # Arguments
/// * `file_name` - Name of file to read. Should be located in package root directory.
pub fn read_input_file_to_str(package_name: &str, file_name: &str) -> String {
    let file_path = format!("{}/{}", package_name, file_name);
    fs::read_to_string(file_path).unwrap()
}

pub fn get_question_part() -> QuestionPart {
    match env::args()
        .nth(1)
        .expect("Missing question part argument.")
        .as_str()
    {
        "1" => return QuestionPart::Part1,
        "2" => return QuestionPart::Part2,
        _ => panic!("Not a valid question part."),
    };
}
