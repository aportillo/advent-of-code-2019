# Advent of Code 2019 in Rust

Solving Advent of Code 2019 in Rust. Advent calendar of small programming puzzles: https://adventofcode.com

## Running Solutions

Run the following in your terminal:

```
# Use the following format: cargo run -p {day##} {part}

λ cargo run -p day01 2
> The answer to part 2 of day01 is 4837367.
```
