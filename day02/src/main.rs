use std::env;
use std::fs;

// https://adventofcode.com/2019/day/2

fn main() {
    let mut intcode = input_to_intcode_vector("input.txt");
    // before running the program, replace position 1 with the value 12
    // and replace position 2 with the value 2.
    intcode[1] = 12;
    intcode[2] = 2;

    let question_part = env::args().nth(1).expect("missing argument");
    let answer: u32;
    match question_part.as_str() {
        "1" => answer = run_intcode_program(intcode)[0],
        "2" => answer = noun_verb_of_target_output(intcode, 19690720),
        _ => panic!("Not a valid question number."),
    }
    println!(
        "The answer to part {} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Read input text and convert to Intcode vector.
fn input_to_intcode_vector(file_name: &str) -> Vec<u32> {
    let file_path = format!("{}/{}", env!("CARGO_PKG_NAME"), file_name);
    let input_file = fs::read_to_string(file_path).unwrap();
    let input_str: &str = &input_file[..];
    let numbers: Vec<u32> = input_str
        .split(",")
        .map(|x| x.parse::<u32>().unwrap())
        .collect();
    numbers
}

/// Run Intcode program and return modified opcodes.
fn run_intcode_program(mut numbers: Vec<u32>) -> Vec<u32> {
    let mut counter = 0;
    for i in 0..numbers.len() {
        if counter == 0 {
            if numbers[i] == 99 {
                break;
            } else if numbers[i] == 1 {
                let index = numbers[i + 3] as usize;
                numbers[index] =
                    numbers[numbers[i + 1] as usize] + numbers[numbers[i + 2] as usize];
                counter = 4;
            } else if numbers[i] == 2 {
                let index = numbers[i + 3] as usize;
                numbers[index] =
                    numbers[numbers[i + 1] as usize] * numbers[numbers[i + 2] as usize];
                counter = 4;
            }
        }
        counter -= 1;
    }
    numbers
}

/// Return 100 * noun (position 1) + verb (position 2) of intcodes needed
/// to return target ouptut (position 0).
fn noun_verb_of_target_output(numbers: Vec<u32>, target: u32) -> u32 {
    // To complete the gravity assist, you need to determine
    // what pair of inputs produces the output 19690720.
    for i in 0..100 {
        for j in 0..100 {
            let mut temp = numbers.clone();
            temp[1] = i;
            temp[2] = j;
            temp = run_intcode_program(temp);
            if temp[0] == target {
                return 100 * i + j;
            }
        }
    }
    0
}
