// https://adventofcode.com/2019/day/2

use std::{env, fs, io};

#[derive(Debug)]
struct Instruction {
    opcode: Opcode,
    parameter_1_mode: ParameterMode,
    parameter_2_mode: ParameterMode,
    parameter_3_mode: ParameterMode,
}

#[derive(Debug, PartialEq, Eq)]
enum Opcode {
    Opcode1,
    Opcode2,
    Opcode3,
    Opcode4,
    Opcode5,
    Opcode6,
    Opcode7,
    Opcode8,
    Opcode99,
}

#[derive(Debug, PartialEq, Eq)]
enum ParameterMode {
    Position,
    Immediate,
}

fn main() {
    let intcode = input_to_intcode_vector("input.txt");
    let question_part = env::args().nth(1).expect("missing argument");
    let mut answer: i32 = 0;
    match question_part.as_str() {
        "1" => {
            println!("Note: Use value 1 for input to part 1.");
            answer = run_intcode_program(intcode)
        }
        "2" => {
            println!("Note: Use value 5 for input to part 2.");
            answer = run_intcode_program(intcode)
        }
        _ => println!("Invalid input for question part number."),
    }
    println!(
        "The answer to part {} of {} is {}.",
        question_part,
        env!("CARGO_PKG_NAME"),
        answer
    );
}

/// Read input text and convert to Intcode vector.
fn input_to_intcode_vector(file_name: &str) -> Vec<i32> {
    let file_path = format!("{}/{}", env!("CARGO_PKG_NAME"), file_name);
    let input_file = fs::read_to_string(file_path).unwrap();
    let input_str: &str = &input_file[..];
    let numbers: Vec<i32> = input_str
        .split(",")
        .map(|x| x.parse::<i32>().unwrap())
        .collect();
    numbers
}

/// Run Intcode program and return modified opcodes.
fn run_intcode_program(mut numbers: Vec<i32>) -> i32 {
    let mut counter = 0;
    let mut final_output = 0;
    let mut index = 0;
    loop {
        if counter == 0 {
            let instruction = instruction_reader(numbers[index]);
            match instruction.opcode {
                Opcode::Opcode1 | Opcode::Opcode2 => {
                    let (result, target_index) =
                        get_instruction_result_and_target_index(&numbers, instruction, index);
                    numbers[target_index] = result;
                    counter = 4;
                }
                Opcode::Opcode3 => {
                    let input: i32 = handle_opcode_3();
                    let target_index = numbers[index + 1] as usize;
                    numbers[target_index] = input;
                    counter = 2;
                }
                Opcode::Opcode4 => {
                    let (result, _) =
                        get_instruction_result_and_target_index(&numbers, instruction, index);
                    println!("Printing output: {}.", result);
                    final_output = result;
                    counter = 2;
                }
                Opcode::Opcode5 | Opcode::Opcode6 => {
                    let (is_true, position) = handle_jump_opcodes(&numbers, instruction, index);
                    if is_true {
                        index = position - 1;
                        counter = 1;
                    } else {
                        counter = 3;
                    }
                }
                Opcode::Opcode7 | Opcode::Opcode8 => {
                    let (value, target_index) =
                        handle_comparison_opcodes(&numbers, instruction, index);
                    numbers[target_index] = value;
                    counter = 4;
                }
                Opcode::Opcode99 => {
                    println!("Reached the end.");
                    break;
                }
            }
        }
        counter -= 1;
        index += 1;
    }
    final_output
}

/// Capture instruction and parameter modes.
///
/// # Example
/// ABCDE
///  1002
///
/// DE - two-digit opcode,      02 == opcode 2
///  C - mode of 1st parameter,  0 == position mode
///  B - mode of 2nd parameter,  1 == immediate mode
///  A - mode of 3rd parameter,  0 == position mode,
fn instruction_reader(instruction: i32) -> Instruction {
    Instruction {
        opcode: match instruction % 10i32.pow(2) {
            1 => Opcode::Opcode1,
            2 => Opcode::Opcode2,
            3 => Opcode::Opcode3,
            4 => Opcode::Opcode4,
            5 => Opcode::Opcode5,
            6 => Opcode::Opcode6,
            7 => Opcode::Opcode7,
            8 => Opcode::Opcode8,
            99 => Opcode::Opcode99,
            _ => panic!("Invalid Opcode."),
        },
        parameter_1_mode: match instruction / 10i32.pow(2) % 10 {
            0 => ParameterMode::Position,
            1 => ParameterMode::Immediate,
            _ => panic!("Invalid parameter mode."),
        },
        parameter_2_mode: match instruction / 10i32.pow(3) % 10 {
            0 => ParameterMode::Position,
            1 => ParameterMode::Immediate,
            _ => panic!("Invalid parameter mode."),
        },
        parameter_3_mode: match instruction / 10i32.pow(4) % 10 {
            0 => ParameterMode::Position,
            1 => ParameterMode::Immediate,
            _ => panic!("Invalid parameter mode."),
        },
    }
}

/// Ask user for input.
fn handle_opcode_3() -> i32 {
    let mut system_id = String::new();
    println!("Input ID of the system to test:");
    io::stdin()
        .read_line(&mut system_id)
        .expect("Failed to read line");
    let system_id: i32 = system_id.trim().parse().expect("Please type a number!");
    system_id
}

/// Handle jump opcodes.
///
/// Opcode 5 is jump-if-true: if the first parameter is non-zero, it sets the instruction
/// pointer to the value from the second parameter. Otherwise, it does nothing.
/// Opcode 6 is jump-if-false: if the first parameter is zero, it sets the instruction
/// pointer to the value from the second parameter. Otherwise, it does nothing.
///
/// # Arguments
/// * `numbers` - Input numbers.
/// * `instruction` - Instruction for input block, opcode and parameter modes.
/// * `index` - Current loop index.
fn handle_jump_opcodes(
    numbers: &Vec<i32>,
    instruction: Instruction,
    index: usize,
) -> (bool, usize) {
    let first_parameter: i32;

    first_parameter = match instruction.parameter_1_mode {
        ParameterMode::Immediate => numbers[index + 1],
        ParameterMode::Position => numbers[numbers[index + 1] as usize],
    };

    if (first_parameter != 0 && instruction.opcode == Opcode::Opcode5)
        || (first_parameter == 0 && instruction.opcode == Opcode::Opcode6)
    {
        match instruction.parameter_2_mode {
            ParameterMode::Immediate => (true, numbers[index + 2] as usize),
            ParameterMode::Position => (true, numbers[numbers[index + 2] as usize] as usize),
        }
    } else {
        (false, 0)
    }
}

/// Handle comparison opcodes.
///
/// Opcode 7 is less than: if the first parameter is less than the second parameter,
/// it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
/// Opcode 8 is equals: if the first parameter is equal to the second parameter,
/// it stores 1 in the position given by the third parameter. Otherwise, it stores 0.
///
/// # Arguments
/// * `numbers` - Input numbers.
/// * `instruction` - Instruction for input block, opcode and parameter modes.
/// * `index` - Current loop index.
/// # Returns
/// * `value` - Either 1 or 0.
/// * `target_index` - Index where to store value.
fn handle_comparison_opcodes(
    numbers: &Vec<i32>,
    instruction: Instruction,
    index: usize,
) -> (i32, usize) {
    let first_parameter: i32;
    let second_parameter: i32;
    let mut value = 0;

    first_parameter = match instruction.parameter_1_mode {
        ParameterMode::Immediate => numbers[index + 1],
        ParameterMode::Position => numbers[numbers[index + 1] as usize],
    };
    second_parameter = match instruction.parameter_2_mode {
        ParameterMode::Immediate => numbers[index + 2],
        ParameterMode::Position => numbers[numbers[index + 2] as usize],
    };

    match instruction.opcode {
        Opcode::Opcode7 => {
            if first_parameter < second_parameter {
                value = 1;
            }
        }
        Opcode::Opcode8 => {
            if first_parameter == second_parameter {
                value = 1;
            }
        }
        _ => panic!("Invalid opcode passed to opcode comparison function."),
    }

    (value, numbers[index + 3] as usize)
}

/// Return resulting value and target index to save value to based on
/// parameter modes (immediate vs position mode).
/// Used for Opcodes 1, 2 and 4.
fn get_instruction_result_and_target_index(
    numbers: &Vec<i32>,
    instruction: Instruction,
    index: usize,
) -> (i32, usize) {
    let parameter_1_value: i32;
    let mut parameter_2_value: i32 = 0;

    parameter_1_value = match instruction.parameter_1_mode {
        ParameterMode::Immediate => numbers[index + 1],
        ParameterMode::Position => numbers[numbers[index + 1] as usize],
    };

    // Handle second parameter, only Opcode1 & Opcode2
    if instruction.opcode != Opcode::Opcode4 {
        parameter_2_value = match instruction.parameter_2_mode {
            ParameterMode::Immediate => numbers[index + 2],
            ParameterMode::Position => numbers[numbers[index + 2] as usize],
        };
    }

    // Return tuple of resulting value and target index to overwrite the value to.
    return match instruction.opcode {
        Opcode::Opcode1 => (
            parameter_1_value + parameter_2_value,
            numbers[index + 3] as usize,
        ),
        Opcode::Opcode2 => (
            parameter_1_value * parameter_2_value,
            numbers[index + 3] as usize,
        ),
        Opcode::Opcode4 => (parameter_1_value, 0 as usize),
        _ => panic!("There was an error."),
    };
}
